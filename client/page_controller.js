var myAppRouter = Backbone.Router.extend({
  routes: {
    "" : "home",
    "*path" : "main"
  },
  home: function() {
    Session.set('page_id', 'home');
  },
  main: function(url_path) {
    Session.set('page_id', url_path);
  }
});
Router = new myAppRouter;
Backbone.history.start({pushState: true});

Template.page_controller.events({
  'click .navlink' : function () {
    // prevent default browser link click behaviour
    event.preventDefault();
    // get the path from the link
    var reg = /.+?\:\/\/.+?(\/.+?)(?:#|\?|$)/;
    var pathname = reg.exec(event.currentTarget.href)[1];
    // route the URL
    Router.navigate(pathname, true);
  }
});

Template.page_controller.display_page = function() {
  var page_id = Session.get('page_id');
  if (page_id.charAt(0) == "/")
    page_id = page_id.substring(1, page_id.length);
  var template = Template[page_id];
  if (!template) template = Template.page_not_found;
  return template();
}