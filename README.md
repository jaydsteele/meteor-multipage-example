meteor-multipage-example
------------------------

A simple sample demonstrating how to create dynamic multi-page applications for Meteor and Node.js
using:

* Backbone.Router
* A Template called page_controller that directs traffic to named child templates
* Two sample child templates: home and help
* A style class called "navlink" that can be used on anchor elements to invoke the router

In essence, this allows you to link to urls like:

    http://myserver.com/home
    http://myserver.com/help

Where home and help are named templates in your Meteor app.

This is based on a blog post by Atma Yogi: http://www.atmayogi.com/2012/11/dynamic-multi-page-applications-with-meteor-for-node-js/

Hope this help others get started quickly with multi-page apps in Meteor.